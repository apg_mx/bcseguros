package Modelo;

import java.util.ArrayList;
import java.util.List;
import java.sql.*;

public class ZurichDAo {
    conexion con;

    public ZurichDAo() {
        con = new conexion();
    }
    
    public List<Zurich> ObtenerLista(int marca, int modelo, int anio){
        List<Zurich> lista = new ArrayList<Zurich>();
        Connection AccesoDB = con.getConexion();
        try {
            PreparedStatement ps = AccesoDB.prepareStatement("select clave, descripcion from vista_catalogo_zurich where idmarca=? and idsubmarca=? and modelo=?");
            ps.setInt(1, marca);
            ps.setInt(2, modelo);
            ps.setInt(3, anio);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Zurich z = new Zurich();
                z.setClave(rs.getString(1));
                z.setDescripcion(rs.getString(2));
                lista.add(z);
            }
        } catch (Exception e) {
        }
        return lista;
    }
    
    
}
