package Modelo;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class AnioDAO {
    conexion conexion;

    public AnioDAO() {
        conexion = new conexion();
    }
    
    public List<Anio> ListaAnio(){
        List<Anio> anio = new ArrayList<Anio>();
        Connection accesoDB = conexion.getConexion();
         try{
            PreparedStatement ps = accesoDB.prepareStatement("SELECT * FROM vista_modelo");
            
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Anio a = new Anio();
                a.setSequence(rs.getInt(1));
                a.setAño(rs.getInt(2));
                anio.add(a);
            }
        }catch(Exception e){
        }
        return anio;
    }
    
    public Anio ObtenerAnio(int idn){
        Anio anio=null;
        Connection accesoDB = conexion.getConexion();
        try{
            PreparedStatement ps = accesoDB.prepareStatement("SELECT * FROM vista_modelo WHERE sequence = ?");
            ps.setInt(1, idn);
            ResultSet rs = ps.executeQuery();
           if(rs.next()){
                anio = new Anio();
                anio.setSequence(rs.getInt(1));
                anio.setAño(rs.getInt(2));
                return anio;
            }
        }catch(Exception e){
        }
        return anio;
    }
}
