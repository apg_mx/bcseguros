package Modelo;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RsaDAO {
    conexion con;

    public RsaDAO() {
       con = new conexion();
    }
    
    public List<Rsa> ObtenerListaRsa(int marca, int modelo, int anio){
        List<Rsa> lista = new ArrayList<Rsa>();
        Connection AccesoDB = con.getConexion();
        try {
            PreparedStatement ps = AccesoDB.prepareStatement("select clave, descripcion from vista_catalogo_rsa where idmarca=? and idsubmarca=? and modelo=?");
            ps.setInt(1, marca);
            ps.setInt(2, modelo);
            ps.setInt(3, anio);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Rsa r = new Rsa();
                r.setClave(rs.getString(1));
                r.setDescripcion(rs.getString(2));
                lista.add(r);
            }
        } catch (Exception e) {
        }
        return lista;
    }
}