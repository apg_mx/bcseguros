package Modelo;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class HdiDAO {
     conexion con;

    public HdiDAO() {
     con = new conexion();
    }
    
    public List<Hdi> ObtenerListaHdi(int marca, int modelo, int anio){
        List<Hdi> lista = new ArrayList<Hdi>();
        Connection AccesoDB = con.getConexion();
        try {
            PreparedStatement ps = AccesoDB.prepareStatement("select clave, descripcion from vista_catalogo_hdi where idmarca=? and idsubmarca=? and modelo=?");
            ps.setInt(1, marca);
            ps.setInt(2, modelo);
            ps.setInt(3, anio);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Hdi h = new Hdi();
                h.setClave(rs.getString(1));
                h.setDescripcion(rs.getString(2));
                lista.add(h);
            }
        } catch (Exception e) {
        }
        return lista;
    }
}
