package Modelo;

public class Descripcion {
    private int anio;
    private int marca;
    private int modelo;
    private int codigo;

    public Descripcion() {
    }
    
    public Descripcion(int anio, int marca, int modelo, int codigo) {
        this.anio = anio;
        this.marca = marca;
        this.modelo = modelo;
        this.codigo = codigo;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public int getMarca() {
        return marca;
    }

    public void setMarca(int marca) {
        this.marca = marca;
    }

    public int getModelo() {
        return modelo;
    }

    public void setModelo(int modelo) {
        this.modelo = modelo;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    
    
}
