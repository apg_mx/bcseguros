package Modelo;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AbaDAO {
    conexion con;

    public AbaDAO() {
    con = new conexion();
    }
    
    public List<Aba> ObtenerListaAba(int marca, int modelo, int anio){
        List<Aba> lista = new ArrayList<Aba>();
        Connection AccesoDB = con.getConexion();
        try {
            PreparedStatement ps = AccesoDB.prepareStatement("select clave, descripcion from vista_catalogo_aba where idmarca=? and idsubmarca=? and modelo=?");
            ps.setInt(1, marca);
            ps.setInt(2, modelo);
            ps.setInt(3, anio);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Aba a = new Aba();
                a.setClave(rs.getString(1));
                a.setDescripcion(rs.getString(2));
                lista.add(a);
            }
        } catch (Exception e) {
        }
        return lista;
    }
}
