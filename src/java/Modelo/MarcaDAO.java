package Modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class MarcaDAO {
    conexion con;

    public MarcaDAO() {
        con = new conexion();
    }
    
    public List<Marca> ListadoMarca(int modelo){
        List<Marca> marca = new ArrayList<Marca>();
        Connection AccesoDB = con.getConexion();
        try {
            PreparedStatement ps = AccesoDB.prepareStatement("SELECT * FROM vista_modelo_marca WHERE modelo=?");
            ps.setInt(1, modelo);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Marca m = new Marca();
                m.setIdmarca(rs.getInt(1));
                m.setMarca(rs.getString(2));
                marca.add(m);
            }
        } catch (Exception e) {
        }
        return marca;
    } 
}
