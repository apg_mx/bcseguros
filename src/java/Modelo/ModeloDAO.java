package Modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ModeloDAO {
    conexion con;

    public ModeloDAO() {
        con = new conexion();
    }
    
    public List<Modelo> ListadoModelo(int marca,int anio){
        List<Modelo> modelo = new ArrayList<Modelo>();
        Connection AccesoDB = con.getConexion();
        try {
            PreparedStatement ps = AccesoDB.prepareStatement("SELECT * from vista_modelo_marca_submarca WHERE idmarca=? AND modelo=?");
            ps.setInt(1, marca);
            ps.setInt(2, anio);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Modelo m = new Modelo();
                m.setIdsubmarca(rs.getInt(3));
                m.setSubmarca(rs.getString(4));
                modelo.add(m);
            }
        } catch (Exception e) {
        }
        return modelo;
    }
}
