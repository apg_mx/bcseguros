/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cotizador;

import Modelo.Hdi;
import Modelo.conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Luis
 */
public class HdiCotizadorDAO {
         conexion con;

         
         public HdiCotizadorDAO(){
              con = new conexion();

         }
         
             public List<HdiCotizador> CotizaHdi(String idAuto){
        List<HdiCotizador> lista = new ArrayList<HdiCotizador>();
        Connection AccesoDB = con.getConexion();
        int idcoche=Integer.parseInt(idAuto.trim());
        try {
            
            PreparedStatement ps = AccesoDB.prepareStatement("select * from hdiautos where idhdi=?");
            ps.setInt(1, idcoche);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                HdiCotizador h = new HdiCotizador();
                h.setZona1(rs.getString(10));
                lista.add(h);
            }
        } catch (Exception e) {
            System.out.println("Excepcionn: "+e);
        }
        return lista;
    }

}
