
package Controlador;

import Modelo.ZurichDAo;
import Modelo.Zurich;
import Modelo.Aba;
import Modelo.AbaDAO;
import Modelo.Descripcion;
import Modelo.Hdi;
import Modelo.HdiDAO;
import Modelo.Rsa;
import Modelo.RsaDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CotizadorControlador extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
            try {
                int anio = Integer.parseInt(request.getParameter("cmbAnio"));
                int marca = Integer.parseInt(request.getParameter("cmbMarca"));
                int modelo = Integer.parseInt(request.getParameter("cmbModelo"));
                int codigo = Integer.parseInt(request.getParameter("txtCodigoPostal"));
                Descripcion d = new Descripcion(anio,marca,modelo,codigo);
                request.setAttribute("Anio", d);
//                Obtenemos el recorset para Zurich
                ZurichDAo zurich = new ZurichDAo();
                List<Zurich> lista = zurich.ObtenerLista(marca, modelo, anio);
                request.setAttribute("Lista", lista);
//                Obtenemos el Recorset para Aba
                AbaDAO aba = new AbaDAO();
                List<Aba> lista2 = aba.ObtenerListaAba(marca, modelo, anio);
                request.setAttribute("AbaLista", lista2);
//                Obtenemos el Recorset para Hdi
                HdiDAO hdi = new HdiDAO();
                List<Hdi> lista3 = hdi.ObtenerListaHdi(marca, modelo, anio);
                request.setAttribute("HdiLista", lista3);
//                Obtenemos el Recorset para HDI
                RsaDAO rsa = new RsaDAO();
                List<Rsa> lista4 = rsa.ObtenerListaRsa(marca, modelo, anio);
                request.setAttribute("RsaLista", lista4);
                request.getRequestDispatcher("cotizador.jsp").forward(request, response);
            } catch (Exception ex) {
            }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
//        PrintWriter out = response.getWriter();
//        int Marca = Integer.parseInt(request.getParameter("Marca"));
//        int Anio = Integer.parseInt(request.getParameter("Anio"));
//        request.setAttribute("Marca", Marca);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
