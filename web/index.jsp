<%@page import="Modelo.Anio"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Inicio | BSeguro</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet"> 
    <link href="css/animate.min.css" rel="stylesheet"> 
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="engine2/style.css" />
	<script type="text/javascript" src="engine2/jquery.js"></script>
        <script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
        <script type="text/javascript" src="js/ajaxSeguros.js"></script>
        <link href="css/style_login.css" rel="stylesheet" type="text/css"/>
        <link href="css/modalcss.css" rel="stylesheet" type="text/css"/>
    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
	
    <!-- Chatra {literal} -->
<script>
    ChatraID = '6ohGxDCGJfugGjGXi';
    (function(d, w, c) {
        var n = d.getElementsByTagName('script')[0],
            s = d.createElement('script');
        w[c] = w[c] || function() {
            (w[c].q = w[c].q || []).push(arguments);
        };
        s.async = true;
        s.src = (d.location.protocol === 'https:' ? 'https:': 'http:')
            + '//call.chatra.io/chatra.js';
        n.parentNode.insertBefore(s, n);
    })(document, window, 'Chatra');
</script>
<!-- /Chatra {/literal} -->


<body>
	<header id="header">      
        <div class="container">
            <div class="row">
                <div class="col-sm-12 overflow">
                   <div class="social-icons pull-right">
                        <ul class="nav nav-pills">
                            <li><a href="https://www.facebook.com/bseguromx"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/bseguro"><i class="fa fa-twitter"></i></a></li>
                           
                        </ul>
                    </div> 
                </div>
             </div>
        </div>
        <div class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="index">
                        <h1><img src="img/BCSeguroLogo.png" alt="logo"></h1>
                    </a>
                    
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="index">Home</a></li>
                        <li class="dropdown active"><a href="#">Secciones<i class="fa fa-angle-down"></i></a>
                            <ul role="menu" class="sub-menu">
                                   <li><a class="active" href="index">Inicio</a></li>
                                <li><a  href="cotizador.html">Cotizador</a></li>
                                <li><a  href="contacto.html">Contacto</a></li> 
                                
                            </ul>
                        </li>                    
                        <li class="dropdown"><a href="blog.html">Blog <i class="fa fa-angle-down"></i></a>
                            <ul role="menu" class="sub-menu">
                                
                            </ul>
                        </li>
                        <li class="dropdown"><a href="portfolio.html">Portfolio <i class="fa fa-angle-down"></i></a>
                            <ul role="menu" class="sub-menu">
                                
                            </ul>
                        </li>                         
                        <li><a  href="login.html">Iniciar Sesion</a></li>
                    </ul>
                </div>
                <div class="search">
                    <form role="form">
                        <i class="fa fa-search"></i>
                        <div class="field-toggle">
                            <input type="text" class="search-form" autocomplete="off" placeholder="Search">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </header>
    <!--/#header-->

   <section id="page-breadcrumb">
        <div class="vertical-center sun">
             <div class="container">
                <div class="row">
                    <div class="action">
                        <div class="col-sm-12">
                            <h2 class="page-header"><b>Asegura tu automóvil en solo unos pasos:</b></h2>
                            
                            
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
   </section>
    <!--/#action-->

   <section id="company-information" class="padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <!-- Start WOWSlider.com BODY section --> <!-- add to the <body> of your page -->
	<div id="wowslider-container1">
	<div class="ws_images"><ul>
		<li><img src="data2/images/zurich.png" alt="Zurich" title="Zurich" id="wows1_0"/></li>
		<li><img src="data2/images/rsa.png" alt="RSA" title="RSA" id="wows1_1"/></li>
		<li><img src="data2/images/hdi.png" alt="HDI" title="HDI" id="wows1_2"/></li>
		<li><a href="#"><img src="data2/images/mapfre.png" alt="" title="Mapfre" id="wows1_3"/></a></li>
		<li><img src="data2/images/aba.png" alt="ABA" title="ABA" id="wows1_4"/></li>
	</ul></div>
<div class="ws_script" style="position:absolute;left:-99%"><a href="#"></a></div>
	<div class="ws_shadow"></div>
	</div>	
	<script type="text/javascript" src="engine2/wowslider.js"></script>
	<script type="text/javascript" src="engine2/script.js"></script>
	<!-- End WOWSlider.com BODY section -->
                </div>
                <div class="col-sm-6 padding-top">
                    <form action=""  method="POST" id="frmCotizador" class="contact" onsubmit="return validacion()" >
                        <fieldset class="contact-inner" style="padding-top: 10px">
      

      <p class="contact-input">
          <h2 class="page-header">Cotiza YA!!!</h2>
        <label for="select" class="select">
          <select name="cmbAnio" id="Anio">
            <option value="" selected>Seleccione Año…</option>
            <% List<Anio> Anio = (List<Anio>) request.getAttribute("ListaFecha");
                for(Anio anio: Anio){
            %>
            <option value="<%=anio.getSequence()%>"><%=anio.getAño()%></option>
            <%}%>
          </select>
        </label>
        <label for="select" class="select">
          <select name="cmbMarca" id="Marca">
            <option value="" selected>Seleccione Marca…</option>
          </select>
        </label>
        <label for="select" class="select">
          <select name="cmbModelo" id="Modelo">
            <option value="" selected>Seleccione Modelo…</option>
          </select>
        </label>
          <input type="text" name="txtCodigoPostal" id="CodigoPostal" placeholder="Codigo Postal" autofocus autocomplete="off">
      </p>
          

      <p class="contact-submit" style="margin-top: 10px">
          <a style="margin-right: 30px; font-size: 15px; " href="#openModal">No encontraste tu automovil?</a><input type="submit" value="Cotizar" id="cotizar" />
          <h2><div id="mensaje"></div></h2>
      </p>
    </fieldset>
  </form>

                </div>
            </div>
        </div>
    </section>

    
	<section id="page-breadcrumb">
        <div class="vertical-center sun">
             <div class="container">
                <div class="row">
                    <div class="action">
                        <div class="col-sm-12">

                            <div id="openModal" class="modalDialog">
                                <div>	<a href="#close" title="Close" class="close">X</a>
                              <form action="#close" class="contact">
    <fieldset class="contact-inner" style="padding-top: 10px">
      

      <p class="contact-input" >
          <h2 class="page-header">No encontraste tu automovil?</h2>

          <h3 class="page-header">Envianos tus datos...</h3>
          
        <label style="width: 100px" for=”nombre”>Nombre:</label>
         <input style="width: 300px" type="text" name="name" placeholder="Nombre" autofocus></br>
         <label style="width: 100px" for=”marca”>Marca:</label>
          <input style="width: 300px" type="text" name="name" placeholder="Marca" autofocus></br>
          <label style="width: 100px" for=”modelo”>Modelo:</label>
         <input style="width: 300px" type="text" name="name" placeholder="Modelo" autofocus></br>
         <label style="width: 100px" for=”anio”>A&ntilde;o:</label>
          <input style="width: 300px" type="text" name="name" placeholder="Año" autofocus></br>
        <label style="width: 100px" for=”correo”>Correo:</label>
        <input style="width: 300px" type="email" name="name" placeholder="Correo" autofocus></br>
        
        
        
        
          

      <p class="contact-submit">
        <input type="submit" value="Enviar">
      </p>
    </fieldset>
  </form>  
                            </div>
                            
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
   </section>
    <!--/#action-->
    

    
	<footer id="footer" style="background: #f9f9f9; ">
        <div class="container">
            <div class="row">
                
                
                <div class="col-sm-12">
                    <div class="copyright-text text-center">
                        <p>&copy; L&R Consultores 2016. All Rights Reserved.</p>
                        
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--/#footer-->

    <script type="text/javascript" src="js/validar.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>   
</body>
</html>
