<%@page import="Modelo.Rsa"%>
<%@page import="Modelo.Hdi"%>
<%@page import="Modelo.Aba"%>
<%@page import="Modelo.Zurich"%>
<%@page import="java.util.List"%>
<%@page import="Modelo.Descripcion"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

    
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Cotizador | BSeguro</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet"> 
    <link href="css/animate.min.css" rel="stylesheet"> 
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css">

    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">

    <link href="engine4/style.css" rel="stylesheet" type="text/css"/>
    <script src="engine4/jquery.js" type="text/javascript"></script>
    <script src="js/hdi.js" type="text/javascript"></script>
	<script src="js/zurich.js" type="text/javascript"></script>
	<script src="js/aba.js" type="text/javascript"></script>
	<script src="js/mapfre.js" type="text/javascript"></script>
<!-- Chatra {literal} -->
<script>
    ChatraID = '6ohGxDCGJfugGjGXi';
    (function(d, w, c) {
        var n = d.getElementsByTagName('script')[0],
            s = d.createElement('script');
        w[c] = w[c] || function() {
            (w[c].q = w[c].q || []).push(arguments);
        };
        s.async = true;
        s.src = (d.location.protocol === 'https:' ? 'https:': 'http:')
            + '//call.chatra.io/chatra.js';
        n.parentNode.insertBefore(s, n);
    })(document, window, 'Chatra');
</script>
<!-- /Chatra {/literal} -->

<body>
	<header id="header">      
        <div class="container">
            <div class="row">
                <div class="col-sm-12 overflow">
                   <div class="social-icons pull-right">
                        <ul class="nav nav-pills">
                            <li><a href="https://www.facebook.com/bseguromx"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/bseguro"><i class="fa fa-twitter"></i></a></li>
                           
                        </ul>
                    </div> 
                </div>
             </div>
        </div>
        <div class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="index">
                        <h1><img src="img/BCSeguroLogo.png" alt="logo"></h1>
                    </a>
                    
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="index">Home</a></li>
                        <li class="dropdown active"><a href="#">Secciones<i class="fa fa-angle-down"></i></a>
                            <ul role="menu" class="sub-menu">
                                <li><a  href="login.html">Login</a></li>
                                <li><a  href="index">Inicio</a></li>
                                <li><a class="active" href="cotizador.html">Cotizador</a></li>
                                <li><a  href="contacto.html">Contacto</a></li>    
                            </ul>
                        </li>                    
                        <li class="dropdown"><a href="blog.html">Blog <i class="fa fa-angle-down"></i></a>
                            <ul role="menu" class="sub-menu">
                                
                            </ul>
                        </li>
                        <li class="dropdown"><a href="portfolio.html">Portfolio <i class="fa fa-angle-down"></i></a>
                            <ul role="menu" class="sub-menu">
                                
                            </ul>
                        </li>                         
                        <li><a href="login.html">Iniciar Sesión</a></li> 
                    </ul>
                </div>
                <div class="search">
                    <form role="form">
                        <i class="fa fa-search"></i>
                        <div class="field-toggle">
                            <input type="text" class="search-form" autocomplete="off" placeholder="Search">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </header>
    <!--/#header-->

   <section id="page-breadcrumb">
        <div class="vertical-center sun">
             <div class="container">
                <div class="row">
                    <div class="action">
                        <div class="col-sm-12">
                            <h1 class="title">Las mejores opciones para:</h1>
                            <% Descripcion d = (Descripcion) request.getAttribute("Anio");%>
                            <p><B><%= d.getMarca()+" "+d.getModelo()+" "+d.getAnio()%></B> </p>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
   </section>
    <!--/#action-->

    <div class="col-md-10" style="alignment-baseline: central">
                          
                       
                    </div>
    
    

   <section id="pricePlans">
		<ul id="plans">
                    <li class="form-group">
                                <input type="submit" name="submit" class="btn btn-submit" value="Cobertura Amplia, AQUI!!!">
                            </li>
			<li class="plan">
                            
				<ul class="planContainer">
					<li class="title"><span> <img src="img/ABA.png"/> </span></li>
                                        <li class="price"><p><span> <select class="" data-width="50px" data-size="5" onChange="cotizaAba(this.value);">
                                            <option value="" selected>Seleccione</option>
                                            <% List<Aba> Aba = (List<Aba>) request.getAttribute("AbaLista");
                                                            for(Aba l: Aba){
                                                                %>
                                                            }
                                                            <option value="<%=l.getClave()%>"><%=l.getDescripcion()%></option>
                                                            <%}%>
                                        </select></span></p> </li>
                                        
					<li>
						<ul class="options">
							<li> Anual </li>
                                                        <li style="border-bottom: 2px solid #e2e2e2;"> <input type="checkbox" /> <div id="abaanual"></div><br /></li>
                                                        <li style="padding-top: 15px"> Semestral </li>
							<li style="border-bottom: 2px solid #e2e2e2; padding-bottom: 3px"> <input type="checkbox" /><div id="abasemestral"></div><br /></li>
                                                        <li style="padding-top: 15px"> Anual sin Intereses</li>
							<li style="border-bottom: 2px solid #e2e2e2;"> <input type="checkbox" /> <div id="abaanual"></div><br /></li>
                                                        
						</ul>
					</li>
					<li class="button" ><a href="formulario.html">Contratar</a></li>
				</ul>
			</li>

			<li class="plan">
				<ul class="planContainer">
					<li class="title"><span> <img src="img/HDI.png"/> </span></li>
					<li class="price"><p><span> <select class="" data-width="50px" data-size="5" onChange="cotizaHDI(this.value)">
                                                        <option value="" selected>Seleccione</option>
                                                        <% List<Hdi> Hdi = (List<Hdi>) request.getAttribute("HdiLista");
                                                            for(Hdi l: Hdi){
                                                                %>
                                                            }
                                                            <option value="<%=l.getClave()%>"><%=l.getDescripcion()%></option>
                                                            <%}%>
                                        </select></span></p> </li>
					<li>
						<ul class="options">
							<li> Anual </li>
                                                        <li style="border-bottom: 2px solid #e2e2e2;"> <input type="checkbox" /> <div id="hdimensual"></div> <br /></li>
                                                        <li style="padding-top: 15px"> Semestral </li>
							<li style="border-bottom: 2px solid #e2e2e2; padding-bottom: 3px"> <input type="checkbox" /><div id="hdisemestral"></div> <br /></li>
                                                        <li style="padding-top: 15px"> Anual sin Intereses</li>
							<li style="border-bottom: 2px solid #e2e2e2;"> <input type="checkbox" /> <div id="hdianual"></div> <br /></li>
						</ul>
					</li>
					<li class="button" ><a href="formulario.html">Contratar</a></li>
				</ul>
			</li>

			<li class="plan">
				<ul class="planContainer">
					<li class="title"><span> <img src="img/Mapfre.png"/> </span></li>
					<li class="price"><p><span> <select class="" onChange="cotizaMapfre(this.value);" >
                                            <option value="0">Opcion 0</option>
                                              <option value="1">Opcion 1</option>
                                        </select></span></p> </li>
					<li>
						<ul class="options">
							<li> Anual </li>
                                                        <li style="border-bottom: 2px solid #e2e2e2;"> <input type="checkbox" /> <div id="mapfremensual"></div>  <br /></li>
                                                        <li style="padding-top: 15px"> Semestral </li>
							<li style="border-bottom: 2px solid #e2e2e2; padding-bottom: 3px"> <input type="checkbox" /> <div id="mapfresemestral"></div>  <br /></li>
                                                        <li style="padding-top: 15px"> Anual sin Intereses</li>
							<li style="border-bottom: 2px solid #e2e2e2;"> <input type="checkbox" /> <div id="mapfreanual"></div>  <br /></li>
						</ul>
					</li>
					<li class="button" ><a href="formulario.html">Contratar</a></li>
				</ul>
			</li>
                        
                            <li class="plan">
				<ul class="planContainer">
					<li class="title"><span> <img src="img/RSA.png"/> </span></li>
					<li class="price"><p><span> <select class="" data-width="50px" data-size="5">
                                            <option value="" selected>Seleccione</option>
                                                        <% List<Rsa> Rsa = (List<Rsa>) request.getAttribute("RsaLista");
                                                            for(Rsa l: Rsa){
                                                                %>
                                                            }
                                                            <option value="<%=l.getClave()%>"><%=l.getDescripcion()%></option>
                                                            <%}%>
                                        </select></span></p> </li>
					<li>
						<ul class="options">
							<li> Anual </li>
                                                        <li style="border-bottom: 2px solid #e2e2e2;"> <input type="checkbox" /> $ 8100,00 <br /></li>
                                                        <li style="padding-top: 15px"> Semestral </li>
							<li style="border-bottom: 2px solid #e2e2e2; padding-bottom: 3px"> <input type="checkbox" /> $ 2100,00 <br /></li>
                                                        <li style="padding-top: 15px"> Anual sin Intereses</li>
							<li style="border-bottom: 2px solid #e2e2e2;"> <input type="checkbox" /> $ 6000,00 <br /></li>
						</ul>
					</li>
					<li class="button" ><a href="formulario.html">Contratar</a></li>
				</ul>
			</li>
                        <li class="plan">
				<ul class="planContainer">
					<li class="title"><span> <img src="img/Zurich.png"/> </span></li>
					<li class="price"><p><span> <select class="" data-width="50px" data-size="5" onChange="cotizaZurich(this.value);">
                                                        <option value="" selected>Seleccione</option>
                                                        <% List<Zurich> Zurich = (List<Zurich>) request.getAttribute("Lista");
                                                            for(Zurich l: Zurich){
                                                                %>
                                                            }
                                                            <option value="<%=d.getAnio()+"|"+l.getClave()%>"><%=l.getDescripcion()%></option>
                                                           
                                                            <%}%>
                                        </select></span></p> </li>
					<li>
						<ul class="options">
							<li> Anual </li>
                                                        <li style="border-bottom: 2px solid #e2e2e2;"> <input type="checkbox" /> <div id="zurichmensual"></div> <br /></li>
                                                        <li style="padding-top: 15px"> Semestral </li>
							<li style="border-bottom: 2px solid #e2e2e2; padding-bottom: 3px"> <input type="checkbox" /> <div id="zurichsemestral"></div> <br /></li>
                                                        <li style="padding-top: 15px"> Anual sin Intereses</li>
							<li style="border-bottom: 2px solid #e2e2e2;"> <input type="checkbox" /> <div id="zurichanual"></div> <br /></li>
						</ul>
					</li>
					<li class="button" ><a href="formulario.html">Contratar</a></li>
				</ul>
			</li>
                        
                    
                        
                         
		</ul> <!-- End ul#plans -->
                
                	
	</section>
    
    
    
    <div id="alert-container">
      
                <div class="row">
                    
                    <div class="col-md-12">
                        <h2 class="page-header"></h2>
                    </div>
                    <div class="col-md-2">
                          
                       
                    </div>

                    <div class="col-md-8">
                        <div class="alert alert-info fade in">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <h4> <B> Condiciones Meses Sin Intereses </B></h4>
                            <p><B> ABA SEGUROS: </B> No aplica</p>
                            <p><B> HDI SEGUROS: </B> 6 Meses sin Intereses con Banamex, Bancomer, HSBC, Scotiabank, American Express y Santander</p>
                            <p><B> RSA SEGUROS: </B> 3 y 6 Meses sin Intereses con American Express, BBVA Bancomer, Banamex Y HSBC</p>
                        
                        </div>
                        
                        
                        
                    </div>
                    
                    <div class="col-md-2">
                          
                       
                    </div>
                </div>
            </div><!--/#alert-container-->

  	<div id="alert-container">
      
                <div class="row">
                    
                    <div class="col-md-12">
                        <h2 class="page-header"></h2>
                    </div>
                    <div class="col-md-2">
                          
                       
                    </div>

                    <div class="col-md-8">
  	<!-- Start WOWSlider.com BODY section -->
<div id="wowslider-container4">
<div class="ws_images"><ul>
		<li><img src="data4/images/segurodecasa.png" alt="Seguro de Casa" title="Seguro de Casa" id="wows4_0"/></li>
		<li><img src="data4/images/segurodemoto.png" alt="Seguro de Moto" title="Seguro de Moto" id="wows4_1"/></li>
		<li><a href="#"><img src="data4/images/gastosmedicosmenores.jpg" alt="" title="Gastos Medicos Menores" id="wows4_2"/></a></li>
		<li><img src="data4/images/segurodeauto.png" alt="Seguro de Auto" title="Seguro de Auto" id="wows4_3"/></li>
	</ul></div>
<div class="ws_script" style="position:absolute;left:-90%"><a> </a> </div>
<div class="ws_shadow"></div>
</div>	
<script type="text/javascript" src="engine4/wowslider.js"></script>
<script type="text/javascript" src="engine4/script.js"></script>
<!-- End WOWSlider.com BODY section -->
                        
                        </div>
                        
                        
                        
                    </div>
                    
                    <div class="col-md-2">
                          
                       
                    </div>
                </div>
            </div><!--/#alert-container-->



    
	<footer id="footer" style="background: #f9f9f9; ">
        <div class="container">
            <div class="row">
                
                
                <div class="col-sm-12">
                    <div class="copyright-text text-center">
                        <p>&copy; L&R Consultores 2016. All Rights Reserved.</p>
                        
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--/#footer-->

    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>   
</body>
</html>

        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center bottom-separator">
                  
                </div>
                <div class="col-md-4 col-sm-5">
                    <div class="testimonial bottom">
                        
                                                
                    </div> 
                </div>
                <div class="col-md-3 col-sm-3 col-md-offset-1">
                    <div class="contact-info bottom">
                        <h2>Contacto</h2>
                        <address>
                        E-mail: <a href="mailto:someone@example.com">email@email.com</a> <br> 
                        Phone: (0155) 63-78-03-26 <br> 
                        Fax: +1 (123) 456 7891 <br> 
                        </address>

                        <h2>Dirección</h2>
                        <address>
                        Unit C2, St.Vincent's Trading Est., <br> 
                        Feeder Road, <br> 
                        Bristol, BS2 0UY <br> 
                        United Kingdom <br> 
                        </address>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="contact-form bottom">
                        <h2>Envie un Mensaje</h2>
                        <form id="main-contact-form" name="contact-form" method="post" action="sendemail.php">
                            <div class="form-group">
                                <input type="text" name="name" class="form-control" required="required" placeholder="Nombre">
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" required="required" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Su mensaje aqui"></textarea>
                            </div>                        
                            <div class="form-group">
                                <input type="submit" name="submit" class="btn btn-submit" value="Enviar">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="copyright-text text-center">
                        <p>&copy; L&R Consultores 2016. All Rights Reserved.</p>
                        
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--/#footer-->

    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>   
</body>
</html>
