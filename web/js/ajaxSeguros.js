$(document).ready(function(){
    $('select[name=cmbAnio]').on('change',function(){
        $.ajax({
            type: 'POST',
            url: 'MarcaControlador',
            data: 'Anio='+$('select[name=cmbAnio]').val(),
            statusCode:{
               404: function(){
                   alert('Pagina no encontrada');
               },
               500: function(){
                   $('select[name=cmbMarca]').append('<option selected>hola</option>');
               }
            },
            success: function(dados){
                $('select[name=cmbMarca] option').remove();
                $('select[name=Modelo] option').remove();
                var pegados = dados.split(":");
                $('select[name=cmbMarca]').append('<option selected>Seleccione Marca…</option>');
                $('select[name=Modelo]').append('<option selected>Seleccione Modelo…</option>');
                for(var i=0;i<pegados.length-1;i++){
                    var sequence = pegados[i].split("-")[0];
                    var nombre = pegados[i].split("-")[1];
                     $('select[name=cmbMarca]').append('<option value="'+sequence+'">'+nombre+'</option>');
                }
            }
        });
    });
    
    $('select[name=cmbMarca]').on('change',function(){
        $.ajax({
            type: 'GET',
            url: 'ModeloControlador',
            data: 'Marca='+$('select[name=cmbMarca]').val()+'&Anio='+$('select[name=cmbAnio]').val(),
            statusCode:{
               404: function(){
                   alert('Pagina no encontrada Modelo');
               },
               500: function(){
                   $('select[name=cmbModelo]').append('<option selected>Seleccione Modelo…</option>');
               }
            },
            success: function(dados1){
                $('select[name=cmbModelo] option').remove();
                var pegados1 = dados1.split(":");
                 $('select[name=cmbModelo]').append('<option selected>Seleccione Modelo…</option>');
                for(var i=0;i<pegados1.length-1;i++){
                    var sequence1 = pegados1[i].split("-")[0];
                    console.log(sequence1);
                    var nombre1 = pegados1[i].split("-")[1];
                    $('select[name=cmbModelo]').append('<option value="'+sequence1+'">'+nombre1+'</option>');
                }
            }
        });
    });
    
     $('select[name=Modelo]').on('change',function(){
//         alert($('select[name=cmbAnio]').val());
     });
    
});

